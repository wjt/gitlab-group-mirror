try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Gitlab Group Mirror',
    'author': 'Will Thompson',
    'url': 'https://gitlab.com/wjt/gitlab-group-mirror',
    'download_url': 'https://gitlab.com/wjt/gitlab-group-mirror',
    'license': "GPL3",
    'author_email': 'will@willthompson.co.uk',
    'version': '0.1',
    'install_requires': ['argh', 'requests'],
    'packages': [],
    'scripts': ['bin/gitlab-group-mirror'],
    'name': 'gitlab-group-mirror',
    'classifiers': [
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
    ],
}

setup(**config)
