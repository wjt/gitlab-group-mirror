Maintains a mirror of all projects in a GitLab group.

## Usage

If you wanted to mirror all of <https://gitlab.com/groups/gitlab-org>'s
repositories to `/srv/gitlab.org`, for example, add this to `mirror.cfg`:

```ini
[gitlab-org]
target_directory = /tmp/gitlab.org
private_token = tOpS3KrEt
```

Then run:

```sh
gitlab-group-mirror gitlab-org
```

Each run will clone any new repositories in the group, and update all others.
It will *not* delete mirrors of repositories removed from the group.

You can find your `PRIVATE_TOKEN` at <https://gitlab.com/profile/account>.

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## A Lament

I have written this script so many times for so many different code-hosting
platforms. ☹
